Team

Diversity and Inclusion are fundamental to the success of GitLab. We aim to make a significant impact in our efforts to foster an environment where everyone can thrive. The Gitlabber community is built upon respect, understanding, tolerance and encouragement. 

The GitLab Diversity and Inclusion team is passionate about implementing and promoting diversity and inclusion throughout GitLab. We are cultivating a workspace for everyone to contribute, engage and support one another. We encourage all GitLabbers to join us! 

GitLab Diversity and Inclusion Team Mission
The GitLab mission is simple, we aim to Ensure GitLab is a welcoming environment for everyone. The team provides an open and accessible platform for team members to come together and engage with one another. 